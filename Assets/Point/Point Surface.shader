// (https://docs.unity3d.com/Manual/SL-SurfaceShaders.html)
// We can use Universal Rendering Pipelines (URP) package else (https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@10.4/manual/InstallURPIntoAProject.html) with friendly UI which generates the shader file for us and has a better compatibility
Shader "Graph/Point Surface" {
    // Get configurations' options in the editor
    Properties {
        _Smoothness ("Smoothness", Range(0,1)) = 0.5
    }

    SubShader {
        // SubShader code written in a hybrid of CG and HLSL enclosed by CGPROGRAM and ENDCG keywords
        CGPROGRAM
            // pragma gives directives for the surface shader
            // pragma surface SURFACEFUNCTION LIGHTMODEL [optionalparams]
            // here : configure surface with standard lighting and full support for shadows
            #pragma surface ConfigureSurface Standard fullforwardshadows
            // Minimum required to get this SubShader supported (https://docs.unity3d.com/Manual/SL-ShaderCompileTargets.html)
            #pragma target 3.0

            // Input should contain any texture coordinates and extra automatic variables needed by surface function
            struct Input {
                // contains world space position of what gets rendered
                float3 worldPos;
            };

            float _Smoothness;

            // Takes in parameters the input and as a second parameter the surface configuration datas (inout because used as the result of the function)
            void ConfigureSurface(Input input, inout SurfaceOutputStandard surface) {
                // 3 values for Albedo : RGB. So it corresponds to x y z axis values.
                // saturate clamps the value so that it is never larger than 1.0 and never smaller than 0.0
                surface.Albedo.rg = saturate(input.worldPos.xy * 0.5 + 0.5);
                surface.Smoothness = _Smoothness;
            }
        ENDCG
    }

    // if none of subshaders can run on this hardware, use the standard diffuse shader
    FallBack "Diffuse"
}