using UnityEngine;

public class Graph : MonoBehaviour
{
    [SerializeField]
    Transform pointPrefab = default;

    [SerializeField, Range(10, 100)]
    int resolution = 10;

    // Array for all of our points
    Transform[] points;

    void Awake() {
        graphInit();
    }
    
    void graphInit() {
        float step = 2f / resolution;
        Vector3 position = Vector3.zero;
        Vector3 scale = Vector3.one * step;
        // Instantiate new Array of points (arrays are objects) having the length of our graph resolution
        points = new Transform[resolution];
        for (int i = 0; i < points.Length; i++) {
            Transform point = Instantiate(pointPrefab);
            // SetParent method allows us to give our point a Parent (here the Graph)
            // it takes in parameters the parent transform and a bool to change or not the parent-relative position/scale/rotation so that the object keeps the same world space as before (not needed here)
            point.SetParent(this.transform, false);
            position.x = (i + 0.5f) * step - 1f;
            point.localPosition = position;
            point.localScale = scale;
            points[i] = point;
        }
    }

    void Update() {
        animatePointsOnYAxis();
    }

    // Adjust the Y points position
    void animatePointsOnYAxis() {
        // current game time
        float time = Time.time;
        for(int i = 0; i < points.Length; i++) {
            Transform point = points[i];
            Vector3 position = point.localPosition;
            // f(x,t) = sin(PI(x + t)) where the is the elapsed game time. X scaled by PI to see the entire repeating pattern on (-1, 1)
            position.y = Mathf.Sin(Mathf.PI * (position.x + time));
            point.localPosition = position;
        }
    }
}   
